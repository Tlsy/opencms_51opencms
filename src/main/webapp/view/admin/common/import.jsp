<link rel="stylesheet" href="<%=request.getContextPath() %>/view/admin/resource/lib/bootstrap-3.2.0/css/bootstrap.min.css"/>
<link rel="stylesheet" href="<%=request.getContextPath() %>/view/admin/resource/lib/bootstrap-iconpicker-1.7.0/bootstrap-iconpicker/css/bootstrap-iconpicker.min.css"/>
<link href="<%=request.getContextPath() %>/view/admin/resource/css/H-ui.min.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/view/admin/resource/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/view/admin/resource/lib/Hui-iconfont/1.0.1/iconfont.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/view/admin/resource/css/style.css" rel="stylesheet" type="text/css" />
<!-- jQuery -->
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/js/H-ui.js"></script> 
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/js/H-ui.admin.js"></script> 
<!-- Bootstrap -->
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/bootstrap-3.2.0/js/bootstrap.min.js"></script>
<!-- Bootstrap-Iconpicker Iconset for Glyphicon -->
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/bootstrap-iconpicker-1.7.0/bootstrap-iconpicker/js/iconset/iconset-glyphicon.min.js"></script>
<!-- Bootstrap-Iconpicker -->
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/bootstrap-iconpicker-1.7.0/bootstrap-iconpicker/js/bootstrap-iconpicker.min.js"></script>
<!-- layer -->
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/layer-v1.9.3/layer/layer.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/js/common.js"></script>
<script type="text/javascript">
layer.config({
	extend: ['skin/seaning/style.css'],
	skin: 'layer-ext-seaning'
});
</script>
<!-- ztree -->
<link rel="stylesheet" href="<%=request.getContextPath() %>/view/admin/resource/lib/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/ztree/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/ztree/js/jquery.ztree.excheck-3.5.js"></script>
<!-- ueditor -->
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/ueditor/ueditor.all.js"></script>
<!-- My97DatePicker -->
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/My97DatePicker/WdatePicker.js"></script>
<!-- uploadify -->
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/uploadify/jquery.uploadify.js"></script>
<link type="text/css" rel="stylesheet"  href="<%=request.getContextPath() %>/view/admin/resource/lib/uploadify/uploadify.css" />
<!-- tab切换 -->
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/jquery.idTabs.min.js"></script>
<!-- 滑块 -->
<link href="<%=request.getContextPath() %>/view/admin/resource/lib/jrange/jquery.range.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/jrange/jquery.range.js"></script>
<!-- 颜色选择器 -->
<link rel="stylesheet" media="screen" type="text/css" href="<%=request.getContextPath() %>/view/admin/resource/lib/cxcolor/css/jquery.cxcolor.css" />
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/cxcolor/jquery.cxcolor.js"></script>
<!-- 代码编辑器 -->
<link rel="stylesheet" media="screen" type="text/css" href="<%=request.getContextPath() %>/view/admin/resource/lib/codemirror/codemirror.css" />
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/codemirror/codemirror.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/codemirror/mode/xml/xml.js"></script>

