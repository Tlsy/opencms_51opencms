<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
<script type="text/javascript">
var setting = {
	async: {
		enable: true,
		url:path+"/admin/channel/simpleNodes",
		autoParam:["id=parent_channel_id"]
	},
	data: {
		simpleData: {
			enable: true
		}
	},
	callback: {
		onClick: onClick
	}
};
$(document).ready(function(){
	$.fn.zTree.init($("#treeDemo"), setting);
});
function onClick(event, treeId, treeNode, clickFlag) {
	$("#iframepage").contents().find("#parent_channel_id").val(treeNode.id);
	$("#iframepage").contents().find("#listForm").submit();
}	
</script>
</head>
<body>
	<form:form action="#" id="listForm" name="listForm">
		<div class="rightinfo" id="rightinfo">
			<div class="panel  panel-default" style="float: left; width: 15%; height:93%;">
				<div class="panel-header">栏目图</div>
				<div class="panel-body">
					<ul id="treeDemo" class="ztree"></ul>
				</div>
			</div>
			<div class="panel  panel-default ml-10" style="float: left; width: 84%; height: 93%;">
				<div class="panel-header">栏目列表</div>
				<div class="panel-body">
					<iframe id="iframepage" src="${path }/admin/channel/list" width="100%" height="86%"></iframe>
				</div>
			</div>
		</div>
	</form:form>
</body>
</html>
