<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript">
var className="";
var classId="";
var setting = {
	async : {
		enable : true,
		url : path + "/admin/channel/simpleNodes",
		autoParam : [ "id=parent_channel_id" ]
	},
	data: {
		simpleData: {
			enable: true
		}
	},
	check : {
		enable : true,
		chkboxType :{"Y" : "s", "N" : "s" }
	},
	callback : {
		onClick : onClick,
		onCheck : onCheck
	}
};
function onClick(e, treeId, treeNode) {
	var zTree = $.fn.zTree.getZTreeObj("treeDemo");
	zTree.checkNode(treeNode, !treeNode.checked, null, true);
	return false;
}
function onCheck(e,treeId,treeNode){
    var treeObj=$.fn.zTree.getZTreeObj("treeDemo");
    var nodes=treeObj.getCheckedNodes(true);
    var moduleIds="";
    var moduleNames="";
    for(var i=0;i<nodes.length;i++){
      moduleIds+=nodes[i].id + ",";
      moduleNames+=nodes[i].name + "_";
    }
    $("#"+classId).val(moduleIds.substring(0, moduleIds.length-1));
    $("#"+className).val(moduleNames.substring(0, moduleNames.length-1));
}
function showMenu(arg1,arg2) {
	className=arg1;
	classId=arg2;
	var cityObj = $("#"+className);
	var cityOffset = $("#"+className).offset();
	$("#menuContent").css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight()-10 + "px"}).slideDown("fast");

	$("body").bind("mousedown", onBodyDown);
}
function hideMenu() {
	$("#menuContent").fadeOut("fast");
	$("body").unbind("mousedown", onBodyDown);
}
function onBodyDown(event) {
	if (!(event.target.id == "menuBtn" || event.target.id == "pName" || event.target.id == "menuContent" || $(event.target).parents("#menuContent").length>0)) {
		hideMenu();
	}
}
$(document).ready(function(){
	$.fn.zTree.init($("#treeDemo"), setting);
});
</script>
<div id="menuContent">
	    <ul id="treeDemo" class="ztree ztreeSty"></ul>
 </div>