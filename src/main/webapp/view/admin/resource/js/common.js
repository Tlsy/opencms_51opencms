//添加或修改页面
function showForm(){
	 var len= arguments.length; 
	 if(len==1){
		 layer.open({
		        type: 2,
		        title: '添加或修改',
		        maxmin: true,
		        shadeClose: false, //点击遮罩关闭层
		        area : ['500px' , '300px'],
		        btn: ['确认'],
		        yes:function(index, layero){
                	var form = layer.getChildFrame('body', index).find('form')[0];//得到form表单
                	var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                	/**
                	 * 判断是否存在check方法，如果有，则执行，用于检查form表单
                	 * if(typeof iframeWin.check=='function'){
                	 *	if(!iframeWin.check()){
                	 *		return false;
                	 *	}
                	 *}
                	 */
                	if(typeof(form)=="undefined") { 
                		layer.closeAll('iframe');
                	}else{
                		addOrUpdateNew(form);
                	}
		        },
		        content:arguments[0]
		 });
	 }else if(len==2){
		 layer.open({
		        type: 2,
		        title: arguments[0],
		        maxmin: true,
		        shadeClose: false, //点击遮罩关闭层
		        area : ['500px' , '300px'],
		        btn: ['确认'],
		        yes:function(index, layero){
                	var form = layer.getChildFrame('body', index).find('form')[0];//得到form表单
                	var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                	if(typeof(form)=="undefined") { 
                		layer.closeAll('iframe');
                	}else{
                		addOrUpdateNew(form);
                	}
		        },
		        content:arguments[1]
		    });
	 }else if(len==4){
		 layer.open({
		        type: 2,
		        title: arguments[0],
		        maxmin: true,
		        shadeClose: false, //点击遮罩关闭层
		        area : [arguments[2] , arguments[3]],
		        btn: ['确认'],
		        yes:function(index, layero){
		        	//重点理解，可以在表单提交之前，条用页面上的参数赋值，表单验证等方法
		        	var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                	//设置图片的值
		        	try {
	            		 if(typeof(eval('iframeWin.setImageParams')) == "function"){
	            			iframeWin.setImageParams();
	            		 }
            	    } catch(e) {
            		     alert("not exist or error");
            	    }
            	    //设置附件的值
            	    try {
	            		 if(typeof(eval('iframeWin.setOptionParams')) == "function"){
	            			iframeWin.setOptionParams();
	            		 }
	           	    } catch(e) {
	           		     alert("not exist or error");
	           	    }
            	    //进行表单的提交
                	var form = layer.getChildFrame('body', index).find('form')[0];//得到form表单
                	if(typeof(form)=="undefined") { 
                		layer.closeAll('iframe');
                	}else{
                		addOrUpdateNew(form);
                	}
		        },
		        content:arguments[1]
		    });
	 }
}
function handle(msg,url){
    	layer.confirm(msg, {icon: 3}, function(index){
    	    $.ajax({  
    	        type : "GET",
    	        url : url,
    	        dataType: 'json',
    	        success : function(result) {
    	        	if(result.success){
    	        		 document.listForm.submit();
    	        		 layer.close(index);
    	        	}
    	        }  
    	    });
    	});
}
//删除
function del(url){
		layer.confirm('你确定要删除吗？', {icon: 11}, function(index){
		    $.ajax({  
		        type : "GET",
		        url : url,
		        dataType: 'json',
		        success : function(result) {
		        	if(result.success){
		        		 document.listForm.submit();
		        		 layer.close(index);
		        	}else{
		        		layer.alert(result.message,{ icon: 4});
		        		layer.close(index);
		        	}
		        }  
		    });
		});
}
//保存方法一
function addOrUpdate(formId,url){
	if(typeof(check)=='function'){
		if(!check()){
			return false;
		}
	}
	$("#"+formId+"").Validform();
	var params=$("#"+formId+"").serializeArray();
	$.ajax({  
        type : "POST",
        url : url,
        data : params,
        dataType: 'json',
        success : function(result) {
        	if(result.success){
        		 parent.listForm.submit();
        		 parent.layer.closeAll('iframe');
        	}else{
        		parent.layer.msg(result.message,{
        		    icon: 2,      //图标
        		    shade: [0],	  //是否遮罩
        		    offset: 'rb', //右下角弹出
        		    time: 2000,   //2秒后自动关闭
        		    shift: 2,     //弹出方式
        		    skin: 'layer-ext-moon' 
        		})
        	}
        }  
    });  
}

//保存方法二
function addOrUpdateNew(form){
	if(!validate(form)){
	  return false;
	}
	var action=$(form).attr('action');
	var params=$(form).serializeArray();
	$.ajax({  
        type : "POST",
        url : action,
        data : params,
        dataType: 'json',
        success : function(result) {
        	if(result.success){
        		 document.listForm.submit();
        		 layer.closeAll('iframe');
        	}else{
        		parent.layer.msg(result.message,{
        		    icon: 2,      //图标
        		    shade: [0],	  //是否遮罩
        		    offset: 'rb', //右下角弹出
        		    time: 2000,   //2秒后自动关闭
        		    shift: 2,     //弹出方式
        		    skin: 'layer-ext-moon' 
        		})
        	}
        }  
    }); 
}
//文件上传
function fileUpload(functionName){
	layer.open({
        type: 2,
        title: '文件上传',
        maxmin: true,
        shadeClose: false, //点击遮罩关闭层
        area : ['400px' , '150px'],
        offset: '100px',
        btn: ['上传'],
        yes:function(index, layero){
        	var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
            iframeWin.sub();	
        },
        content:path+'/view/common/fileUpload.jsp?functionName='+functionName
    }); 
}

//文件上传并裁剪
function fileUploadAndCut(){
	layer.open({
        type: 2,
        title: '文件上传',
        maxmin: true,
        shadeClose: false, //点击遮罩关闭层
        area : ['700px' , '350px'],
        offset: '0px',
        btn: ['上传'],
        yes:function(index, layero){
        	var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
        	iframeWin.sub();
        	},
        content:path+'/view/common/fileUploadAndCut.jsp'
    }); 
}
/**
 * 获取url参数
 * @param name
 * @returns
 */
function getParams(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  unescape(r[2]); return null;
}
/**
 * 验证表单
 * @param form
 * @returns {Boolean}
 */
function validate(form){
	var count=0;
	var inputs=$(form).find("input");
	//清楚错误提示
	$(form).find(".errorSpan").remove();
	$(inputs).removeClass("errorInput");
	for(var i=0;i<inputs.length;i++){
		var input=inputs[i];
		if($(input).attr("datatype")){
			var datatypes=$(input).attr("datatype").split(" ");
			var errormsgs=$(input).attr("errormsg").split(" ");
			var val=$(input).val();
			for(var j=0;j<datatypes.length;j++){
				if("required"==datatypes[j]){
					if(val==""){
						$(input).addClass("errorInput");
						$(input).attr('placeholder',errormsgs[j]);
						//$(input).parent().append("<span class='errorSpan' style='color:red'>"+errormsgs[j]+"</span>");
						count++;
						break;
					}
				}
			}
		}
	}
	if(count==0){
		return true;
	}else{
		return false;
	}
	
}
