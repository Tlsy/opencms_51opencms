<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
<script type="text/javascript">
function fileUploadSuccess(file){
	$("#imgUrl").val(file);
}
</script>
</head>
<body>
    <form:form id="form_add_update" action="${path}/admin/friendLink/addOrUpdate" name="form_add_update" modelAttribute="pageForm">
      <form:hidden path="id" />
      <table class='formtable'>
        <tr>
          <td colspan="4"><div class="formtitle"><span>基本信息</span></div></td>
        </tr>
       <tr>
          <td>名称</td>
          <td><form:input path="name" cssClass="dfinput" datatype="required"  errormsg="名称不能为空"/></td>
           <td>类型</td>
          <td>
              <form:select path="type" cssClass="select2">
                  <form:options items="${FRIEND_LINK_TYPE_LIST }" itemValue="value" itemLabel="label" />
              </form:select>
          </td>
       </tr>
       <tr>
          <td>图片</td>
          <td>
             <form:input path="imgUrl" cssClass="dfinput"/>
             <a href="javascript:fileUpload('fileUploadSuccess');" class="add">图片上传</a>
          </td>
          <td>链接地址</td>
          <td><form:input path="url" cssClass="dfinput" datatype="required"  errormsg="链接地址不能为空"/></td>
       </tr>
       </table>
     </form:form>
</body>
</html>
