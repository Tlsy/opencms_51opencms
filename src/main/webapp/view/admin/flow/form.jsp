<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
</head>
<body>
    <form:form action="${path}/admin/flow/addOrUpdate" id="form_add_update" name="form_add_update" modelAttribute="pageForm">
       <form:hidden path="id" />
      <table class='formtable'>
        <tr>
          <td colspan="4"><div class="formtitle"><span>基本信息</span></div></td>
        </tr>
       <tr>
          <td>名称</td>
          <td><form:input path="name" cssClass="dfinput"/></td>
          <td>排序</td>
          <td><form:input path="sort" cssClass="dfinput"/></td>
       </tr>
       <tr>
          <td>是否开放</td>
          <td>
                                             是&nbsp;<form:radiobutton path="isOpen" value="1"  />&nbsp;&nbsp;&nbsp;
                                             否&nbsp;<form:radiobutton path="isOpen" value="2" />
          </td>
       </tr>
       <tr>
          <td>描述</td>
          <td><form:input path="description" cssClass="dfinput"/></td>
       </tr>
       </table>
     </form:form>
</body>
</html>
