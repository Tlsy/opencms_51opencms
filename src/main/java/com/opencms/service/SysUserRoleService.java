package com.opencms.service;
import com.opencms.bean.SysUserRole;
import com.opencms.service.base.ServiceSupport;
public interface SysUserRoleService extends ServiceSupport<SysUserRole> {
}
