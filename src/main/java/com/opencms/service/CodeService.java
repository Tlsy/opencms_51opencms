package com.opencms.service;

import com.opencms.vo.CodeVO;
import com.opencms.vo.QueryResult;



public interface CodeService {

	QueryResult<CodeVO> getBeanList();

	void createDao(String clazzName);

	void createDaoImpl(String clazzName);

	void createService(String clazzName);

	void createServiceImpl(String clazzName);

	void createController(String clazzName);

	void createListView(String clazzName);

	void createFormView(String clazzName);


}
