package com.opencms.service;
import com.opencms.bean.SysUser;
import com.opencms.service.base.ServiceSupport;
public interface SysUserService extends ServiceSupport<SysUser> {
	void addOrUpdate(SysUser sysUser,String roleIds);
}
