package com.opencms.service;
import com.opencms.bean.ContentOption;
import com.opencms.service.base.ServiceSupport;
public interface ContentOptionService extends ServiceSupport<ContentOption> {
}
