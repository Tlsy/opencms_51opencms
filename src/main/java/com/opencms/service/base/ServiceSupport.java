package com.opencms.service.base;

import java.util.Map;

import com.opencms.vo.QueryResult;

public interface ServiceSupport<T> {

	void insert(T clazz);

	void insertSelective(T clazz);

	void deleteByPrimaryKey(Integer id);

	void updateByPrimaryKey(T clazz);

	void updateByPrimaryKeySelective(T clazz);
	
	void saveOrUpdate(T clazz);

	T selectByPrimaryKey(Integer id);
	
	QueryResult<T> list();

	QueryResult<T> list(Map<String, Object> params);
	
	QueryResult<T> list(T clazz);
	
	


}