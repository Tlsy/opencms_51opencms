package com.opencms.service;
import com.opencms.bean.AdvertPosition;
import com.opencms.service.base.ServiceSupport;
public interface AdvertPositionService extends ServiceSupport<AdvertPosition> {
}
