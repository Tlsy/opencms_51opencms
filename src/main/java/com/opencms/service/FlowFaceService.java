package com.opencms.service;
import com.opencms.bean.FlowFace;
import com.opencms.service.base.ServiceSupport;
public interface FlowFaceService extends ServiceSupport<FlowFace> {
}
