package com.opencms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.ContentVideo;
import com.opencms.dao.ContentVideoMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.ContentVideoService;
import com.opencms.service.base.impl.ServiceSupportImpl;
@Service
public class ContentVideoServiceImpl extends ServiceSupportImpl<ContentVideo> implements ContentVideoService {

	@Autowired
	private ContentVideoMapper contentVideoMapper;

	@Override
	public MapperSupport<ContentVideo> getMapperSupport() {
		return contentVideoMapper;
	}
	
	
}
