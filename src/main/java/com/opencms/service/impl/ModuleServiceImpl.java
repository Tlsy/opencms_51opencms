package com.opencms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.Module;
import com.opencms.dao.ModuleMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.ModuleService;
import com.opencms.service.base.impl.ServiceSupportImpl;

@Service
public class ModuleServiceImpl extends ServiceSupportImpl<Module>implements ModuleService {
	@Autowired
	private ModuleMapper moduleMapper;

	@Override
	public MapperSupport<Module> getMapperSupport() {
		return moduleMapper;
	}
}
