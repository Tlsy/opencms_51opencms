package com.opencms.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.Flow;
import com.opencms.dao.FlowMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.FlowService;
import com.opencms.service.base.impl.ServiceSupportImpl;
@Service
public class FlowServiceImpl extends ServiceSupportImpl<Flow> implements FlowService {
   @Autowired
   private FlowMapper flowMapper;

@Override
public MapperSupport<Flow> getMapperSupport() {
	return flowMapper;
}
}
