package com.opencms.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.Role;
import com.opencms.dao.RoleMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.RoleService;
import com.opencms.service.base.impl.ServiceSupportImpl;
@Service
public class RoleServiceImpl extends ServiceSupportImpl<Role> implements RoleService {
   @Autowired
   private RoleMapper roleMapper;

@Override
public MapperSupport<Role> getMapperSupport() {
	// TODO Auto-generated method stub
	return roleMapper;
}
}
