package com.opencms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.SysUserRole;
import com.opencms.dao.SysUserRoleMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.SysUserRoleService;
import com.opencms.service.base.impl.ServiceSupportImpl;

@Service
public class SysUserRoleServiceImpl extends ServiceSupportImpl<SysUserRole>implements SysUserRoleService {
	@Autowired
	private SysUserRoleMapper sysUserRoleMapper;

	@Override
	public MapperSupport<SysUserRole> getMapperSupport() {
		return sysUserRoleMapper;
	}

}
