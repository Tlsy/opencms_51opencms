package com.opencms.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.DictType;
import com.opencms.dao.DictTypeMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.DictTypeService;
import com.opencms.service.base.impl.ServiceSupportImpl;
@Service
public class DictTypeServiceImpl extends ServiceSupportImpl<DictType> implements DictTypeService {
   @Autowired
   private DictTypeMapper dictTypeMapper;

@Override
public MapperSupport<DictType> getMapperSupport() {
	return dictTypeMapper;
}
}
