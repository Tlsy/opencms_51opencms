package com.opencms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencms.bean.ContentText;
import com.opencms.dao.ContentTextMapper;
import com.opencms.dao.base.MapperSupport;
import com.opencms.service.ContentTextService;
import com.opencms.service.base.impl.ServiceSupportImpl;
@Service
public class ContentTextServiceImpl extends ServiceSupportImpl<ContentText> implements ContentTextService {

	@Autowired
	private ContentTextMapper contentTextMapper;

	@Override
	public MapperSupport<ContentText> getMapperSupport() {
		return contentTextMapper;
	}
	
	
}
