package com.opencms.service;
import com.opencms.bean.ContentText;
import com.opencms.service.base.ServiceSupport;
public interface ContentTextService extends ServiceSupport<ContentText> {
}
