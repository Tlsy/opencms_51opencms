package com.opencms.service;
import com.opencms.bean.Model;
import com.opencms.service.base.ServiceSupport;
public interface ModelService extends ServiceSupport<Model> {
}
