package com.opencms.service;
import com.opencms.bean.ContentReptile;
import com.opencms.service.base.ServiceSupport;
public interface ContentReptileService extends ServiceSupport<ContentReptile> {

	void toContent(String ids, String channelId) throws Exception;
}
