package com.opencms.controller.admin;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.opencms.bean.Role;
import com.opencms.controller.base.AdminBaseController;
import com.opencms.service.ModuleService;
import com.opencms.service.RoleService;
import com.opencms.vo.ActionResult;
import com.opencms.vo.QueryResult;
@Controller
@RequestMapping("/admin/role/")
@SessionAttributes("pageForm")
public class RoleController extends AdminBaseController{

     @Autowired
     private RoleService roleService;
     @Autowired
     private ModuleService moduleService;

     @RequestMapping("list")
     public ModelAndView list(Role role,HttpServletRequest request) throws Exception{
        QueryResult<Role> queryResult=roleService.list(role);
        role.setPageDate(queryResult.getQueryResult());
        role.setTotalCount(queryResult.getCount());
        ModelAndView mnv = new ModelAndView();
        mnv.addObject(DEFAULT_PAGE_VIEW,role);
        mnv.setViewName("view/admin/role/list");
        return mnv;
     }

     @RequestMapping("delete")
     @ResponseBody
     public ActionResult delete(Role role,HttpServletRequest request) throws Exception{
        ActionResult result=new ActionResult();
       try {
	     roleService.deleteByPrimaryKey(role.getId());
	     result.setSuccess(true);
	     result.setMessage(RESULE_SUCCESS_DELETE);
		 } catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR_DELETE);
		}
		return result;
	 }

	 @RequestMapping("showForm")
	 public ModelAndView addForm(Role role,HttpServletRequest request) throws Exception{
		ModelAndView mnv = new ModelAndView();
		if(role.getId()!=null){
			role=roleService.selectByPrimaryKey(role.getId());
		}
		mnv.addObject(DEFAULT_PAGE_FORM, role);
		mnv.setViewName("view/admin/role/form");
		return mnv;
	 }

	@RequestMapping("addOrUpdate")
	@ResponseBody
	public ActionResult add(Role role,HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			roleService.saveOrUpdate(role);
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}
}
