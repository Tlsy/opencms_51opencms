package com.opencms.controller.admin;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.opencms.bean.Flow;
import com.opencms.bean.FlowFace;
import com.opencms.bean.Role;
import com.opencms.controller.base.AdminBaseController;
import com.opencms.service.FlowFaceService;
import com.opencms.service.FlowService;
import com.opencms.service.RoleService;
import com.opencms.vo.ActionResult;
import com.opencms.vo.QueryResult;
@Controller
@RequestMapping("/admin/flowFace/")
@SessionAttributes("pageForm")
public class FlowFaceController extends AdminBaseController{

     @Autowired
     private FlowFaceService flowFaceService;
     @Autowired
     private FlowService flowService;
     @Autowired
     private  RoleService roleService;

     @RequestMapping("list")
     public ModelAndView list(FlowFace flowFace,HttpServletRequest request) throws Exception{
        QueryResult<FlowFace> queryResult=flowFaceService.list(flowFace);
        flowFace.setPageDate(queryResult.getQueryResult());
        flowFace.setTotalCount(queryResult.getCount());
        ModelAndView mnv = new ModelAndView();
        mnv.addObject(DEFAULT_PAGE_VIEW,flowFace);
        mnv.setViewName("view/admin/flowFace/list");
        return mnv;
     }
     
     @RequestMapping("getFlowFace")
     @ResponseBody
     public ActionResult getFlowFace(String id,HttpServletRequest request) throws Exception{
    	ActionResult result=new ActionResult();
    	Map<String, Object> params=new HashMap<String, Object>();
    	params.put("flowId", id);
    	params.put("orderBy", "sort asc");
 		QueryResult<FlowFace> queryResult=flowFaceService.list();
 		result.setSuccess(true);
 		result.setResult(queryResult.getQueryResult());
        return result;
     }
     
     @RequestMapping("up")
     @ResponseBody
     public ActionResult up(String flowFaceId,String flowId,HttpServletRequest request) throws Exception{
    	ActionResult result=new ActionResult();
    	Map<String, Object> params=new HashMap<String, Object>();
    	params.put("flowId", flowId);
    	params.put("orderBy", "");
 		QueryResult<FlowFace> queryResult=flowFaceService.list(params);
 		Integer current_index=0;
 		Integer current_sort=0;
 		Integer before_sort=0;
 		for (int i=0;i<queryResult.getQueryResult().size();i++) {
 			FlowFace flowFace=queryResult.getQueryResult().get(i);
			if(flowFaceId.equals(flowFace.getId().toString())){
				current_index=i;
				current_sort=flowFace.getSort();
			}
		}
 		if(current_index==0){
 			result.setSuccess(false);
 	 		result.setMessage("已经是第一个节点");
 	 		 return result;
 		}
 		//前一个节点
 		FlowFace before_flowFace=queryResult.getQueryResult().get(current_index-1);
 		before_sort=before_flowFace.getSort();
 		before_flowFace.setSort(current_sort);
 		//当前节点
 		FlowFace current_flowFace=queryResult.getQueryResult().get(current_index);
 		current_flowFace.setSort(before_sort);
 		flowFaceService.saveOrUpdate(before_flowFace);
 		flowFaceService.saveOrUpdate(current_flowFace);
 		result.setSuccess(true);
        return result;
     }

     @RequestMapping("delete")
     @ResponseBody
     public ActionResult delete(FlowFace flowFace,HttpServletRequest request) throws Exception{
        ActionResult result=new ActionResult();
       try {
	     flowFaceService.deleteByPrimaryKey(flowFace.getId());
	     result.setSuccess(true);
	     result.setMessage(RESULE_SUCCESS_DELETE);
		 } catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR_DELETE);
		}
		return result;
	 }

	 @RequestMapping("showForm")
	 public ModelAndView addForm(FlowFace flowFace,HttpServletRequest request) throws Exception{
		ModelAndView mnv = new ModelAndView();
		QueryResult<Flow> queryResult=flowService.list();
		QueryResult<Role> queryResult2=roleService.list();
		if(flowFace.getId()!=null){
			flowFace=flowFaceService.selectByPrimaryKey(flowFace.getId());
		}else{
			String flow_id=request.getParameter("flow_id");
			if(StringUtils.isNotBlank(flow_id)){
				Flow flow=flowService.selectByPrimaryKey(Integer.valueOf(flow_id));
				Integer maxSort=0;
				Set<FlowFace> set=null;
				//Set<FlowFace> set=flow.getFlowFaces();
				for (FlowFace flowFace2 : set) {
					if(flowFace2.getSort()>maxSort){
						maxSort=flowFace2.getSort();
					}
				}
				flowFace.setSort(maxSort+1);
				flowFace.setFlowId(Integer.valueOf(flow_id));
			}
		}
		mnv.addObject(DEFAULT_PAGE_FORM, flowFace);
		mnv.addObject("FLOW_LIST", queryResult.getQueryResult());
		mnv.addObject("ROLE_LIST", queryResult2.getQueryResult());
		mnv.setViewName("view/admin/flowFace/form");
		return mnv;
	 }

	@RequestMapping("addOrUpdate")
	@ResponseBody
	public ActionResult add(FlowFace flowFace,HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			flowFaceService.saveOrUpdate(flowFace);
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}
}
