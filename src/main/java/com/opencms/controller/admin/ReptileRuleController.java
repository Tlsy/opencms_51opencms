package com.opencms.controller.admin;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.opencms.bean.ReptileRule;
import com.opencms.controller.base.AdminBaseController;
import com.opencms.service.ContentReptileService;
import com.opencms.service.ReptileRuleService;
import com.opencms.utils.reptile.Reptile;
import com.opencms.vo.ActionResult;
import com.opencms.vo.QueryResult;
@Controller
@RequestMapping("/admin/reptileRule/")
@SessionAttributes("pageForm")
public class ReptileRuleController extends AdminBaseController{

     @Autowired
     private ReptileRuleService reptileRuleService;
     @Autowired
     private ContentReptileService contentReptileService;

     @RequestMapping("list")
     public ModelAndView list(ReptileRule reptileRule,HttpServletRequest request) throws Exception{
        QueryResult<ReptileRule> queryResult=reptileRuleService.list(reptileRule);
        reptileRule.setPageDate(queryResult.getQueryResult());
        reptileRule.setTotalCount(queryResult.getCount());
        ModelAndView mnv = new ModelAndView();
        mnv.addObject(DEFAULT_PAGE_VIEW,reptileRule);
        mnv.setViewName("view/admin/reptileRule/list");
        return mnv;
     }

     @RequestMapping("delete")
     @ResponseBody
     public ActionResult delete(ReptileRule reptileRule,HttpServletRequest request) throws Exception{
        ActionResult result=new ActionResult();
       try {
    	  reptileRuleService.deleteByPrimaryKey(reptileRule.getId());
	     result.setSuccess(true);
	     result.setMessage(RESULE_SUCCESS_DELETE);
		 } catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR_DELETE);
		}
		return result;
	 }

	 @RequestMapping("showForm")
	 public ModelAndView addForm(ReptileRule reptileRule,HttpServletRequest request) throws Exception{
		ModelAndView mnv = new ModelAndView();
		if(reptileRule.getId()!=null){
			reptileRule=reptileRuleService.selectByPrimaryKey(reptileRule.getId());
		}else{
			reptileRule.setCoding("UTF-8");
			reptileRule.setTimeFormat("yyyy-MM-dd HH:mm:ss");
			reptileRule.setNumber(500);
		}
		mnv.addObject(DEFAULT_PAGE_FORM, reptileRule);
		mnv.setViewName("view/admin/reptileRule/form");
		return mnv;
	 }

	@RequestMapping("addOrUpdate")
	@ResponseBody
	public ActionResult add(ReptileRule reptileRule,HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			reptileRuleService.saveOrUpdate(reptileRule);
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}
	
	@RequestMapping("reptile")
	@ResponseBody
	public ActionResult reptile(ReptileRule reptileRule,HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			reptileRule=reptileRuleService.selectByPrimaryKey(reptileRule.getId());
			//创建线程，开始抓取数据
			Reptile reptile=new Reptile(reptileRule,contentReptileService);
			Thread thread=new Thread(reptile);
			thread.start();
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			System.out.println(e.toString());
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}
}
