package com.opencms.controller.admin;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.opencms.bean.Flow;
import com.opencms.controller.base.AdminBaseController;
import com.opencms.service.FlowService;
import com.opencms.vo.ActionResult;
import com.opencms.vo.QueryResult;
@Controller
@RequestMapping("/admin/flow/")
@SessionAttributes("pageForm")
public class FlowController extends AdminBaseController{

     @Autowired
     private FlowService flowService;

     @RequestMapping("list")
     public ModelAndView list(Flow flow,HttpServletRequest request) throws Exception{
    	 QueryResult<Flow> queryResult=flowService.list(flow);
        flow.setPageDate(queryResult.getQueryResult());
        flow.setTotalCount(queryResult.getCount());
        ModelAndView mnv = new ModelAndView();
        mnv.addObject(DEFAULT_PAGE_VIEW,flow);
        mnv.setViewName("view/admin/flow/list");
        return mnv;
     }

     @RequestMapping("delete")
     @ResponseBody
     public ActionResult delete(Flow flow,HttpServletRequest request) throws Exception{
        ActionResult result=new ActionResult();
       try {
    	   flowService.deleteByPrimaryKey(flow.getId());
	     result.setSuccess(true);
	     result.setMessage(RESULE_SUCCESS_DELETE);
		 } catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR_DELETE);
		}
		return result;
	 }

	 @RequestMapping("showForm")
	 public ModelAndView addForm(Flow flow,HttpServletRequest request) throws Exception{
		ModelAndView mnv = new ModelAndView();
		if(flow.getId()!=null){
			flow=flowService.selectByPrimaryKey(flow.getId());
		}
		mnv.addObject(DEFAULT_PAGE_FORM, flow);
		mnv.setViewName("view/admin/flow/form");
		return mnv;
	 }

	@RequestMapping("addOrUpdate")
	@ResponseBody
	public ActionResult add(Flow flow,HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			flowService.saveOrUpdate(flow);
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}
}
