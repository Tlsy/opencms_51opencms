package com.opencms.controller.base;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.opencms.bean.FlowFace;
import com.opencms.bean.SysUserRole;
import com.opencms.service.FlowFaceService;
import com.opencms.utils.SessionKey;
import com.opencms.vo.QueryResult;


public abstract class AdminBaseController extends FreemakerController {
	
    public static final String RESULE_SUCCESS="操作成功！";
	
	public static final String RESULE_ERROR="操作失败！";
	
	public static final String RESULE_SUCCESS_DELETE="删除成功！";
	
	public static final String RESULE_ERROR_DELETE="删除失败！";

	public static final String DEFAULT_PAGE_VIEW="pageView";
	
	public static final String DEFAULT_PAGE_FORM="pageForm";
	
	public String  getStatus(Integer flowId,FlowFaceService flowFaceService,HttpServletRequest request){
		List<SysUserRole> sysUserRoles=(List<SysUserRole>) request.getSession().getAttribute(SessionKey.SYS_ROLE);
		String whereSql=" flow="+flowId+" ";
 		QueryResult<FlowFace> queryResult=flowFaceService.list();
 		StringBuffer status=new StringBuffer();
 		for (FlowFace flowFace : queryResult.getQueryResult()) {
 			String roleIds=flowFace.getRoleIds();
 			for (SysUserRole sysUserRole : sysUserRoles) {
				if(roleIds.contains("("+sysUserRole.getRoleId()+")")){
					status.append(flowFace.getId()).append(",");
					break;
				}
				
			}
		}
 		if(StringUtils.isNotBlank(status.toString())){
 			status.deleteCharAt(status.length()-1);
 		}
		return status.toString();
	}


}
