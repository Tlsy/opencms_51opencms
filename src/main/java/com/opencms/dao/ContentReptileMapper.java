package com.opencms.dao;

import com.opencms.bean.ContentReptile;
import com.opencms.dao.base.MapperSupport;

public interface ContentReptileMapper extends MapperSupport<ContentReptile> {
}