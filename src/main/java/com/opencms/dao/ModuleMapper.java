package com.opencms.dao;

import com.opencms.bean.Module;
import com.opencms.dao.base.MapperSupport;

public interface ModuleMapper extends MapperSupport<Module> {
}