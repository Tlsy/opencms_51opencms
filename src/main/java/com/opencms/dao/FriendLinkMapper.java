package com.opencms.dao;

import com.opencms.bean.FriendLink;
import com.opencms.dao.base.MapperSupport;

public interface FriendLinkMapper extends MapperSupport<FriendLink> {
}