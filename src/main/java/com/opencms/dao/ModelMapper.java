package com.opencms.dao;

import com.opencms.bean.Model;
import com.opencms.dao.base.MapperSupport;

public interface ModelMapper extends MapperSupport<Model> {
}