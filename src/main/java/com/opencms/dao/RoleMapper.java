package com.opencms.dao;

import com.opencms.bean.Role;
import com.opencms.dao.base.MapperSupport;

public interface RoleMapper extends MapperSupport<Role> {
}