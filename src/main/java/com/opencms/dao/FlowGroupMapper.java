package com.opencms.dao;

import com.opencms.bean.FlowGroup;
import com.opencms.dao.base.MapperSupport;

public interface FlowGroupMapper extends MapperSupport<FlowGroup> {
}