package com.opencms.dao;

import com.opencms.bean.AdvertPosition;
import com.opencms.dao.base.MapperSupport;

public interface AdvertPositionMapper extends MapperSupport<AdvertPosition> {

}