package com.opencms.dao;

import com.opencms.bean.Channel;
import com.opencms.dao.base.MapperSupport;

public interface ChannelMapper extends MapperSupport<Channel>  {

	Channel selectByCode(String code);

}