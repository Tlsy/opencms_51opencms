package com.opencms.dao;

import com.opencms.bean.ContentVideo;
import com.opencms.dao.base.MapperSupport;

public interface ContentVideoMapper extends MapperSupport<ContentVideo> {
}