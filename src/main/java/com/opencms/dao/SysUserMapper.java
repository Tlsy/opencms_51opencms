package com.opencms.dao;

import com.opencms.bean.SysUser;
import com.opencms.dao.base.MapperSupport;

public interface SysUserMapper extends MapperSupport<SysUser> {
}