package com.opencms.dao;

import com.opencms.bean.ReptileRule;
import com.opencms.dao.base.MapperSupport;

public interface ReptileRuleMapper extends MapperSupport<ReptileRule> {
}