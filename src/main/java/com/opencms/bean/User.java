package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class User extends PageView<User> {
    private Integer id;

    private String createTime;

    private String lastLoginTime;

    private String loginName;

    private String password;

    private String realName;

    private String role;

    private Integer age;

    private String name;

}