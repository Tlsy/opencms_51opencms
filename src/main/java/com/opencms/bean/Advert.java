package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class Advert extends PageView<Advert> {
    private Integer id;

    private Integer clickNum;

    private String createTime;

    private String endTime;

    private String imgUrl;

    private String startTime;

    private String title;

    private String url;

    private Integer advertPositionId;
    
    private String advertPositionName;

}