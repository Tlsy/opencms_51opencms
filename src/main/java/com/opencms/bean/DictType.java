package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class DictType extends PageView<DictType> {
    private Integer id;

    private String chName;

    private String enName;

}