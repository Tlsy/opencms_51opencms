package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class ContentOption extends PageView<ContentOption> {
    private Integer id;

    private String title;

    private Integer clickNum;

    private String optionUrl;

    private Integer status;

    private Integer contentId;

}