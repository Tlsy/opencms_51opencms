package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class Dict extends PageView<Dict> {
    private Integer id;

    private String name;

    private Integer sort;

    private Integer dictTypeId;
    
    private String enName;
    
    private String chName;

}