package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class Role extends PageView<Role> {
    private Integer id;

    private String name;

    private String moduleIds;

    private String moduleNames;

}