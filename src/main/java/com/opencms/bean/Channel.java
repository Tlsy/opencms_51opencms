package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class Channel extends PageView<Channel> {
    private Integer id;

    private String name;

    private Integer pId;

    private String code;

    private String sort;

    private Integer isNav;
    
    private String pName;

    private Boolean isParent;
    
    private Boolean open;
    
    private String imgUrl;
    
    private String template;
    
    private Integer isOut;
    
    private String outUrl;

}