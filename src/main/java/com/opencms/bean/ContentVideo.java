package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class ContentVideo extends PageView<ContentVideo> {
    private Integer id;

    private String title;

    private Integer clickNum;

    private String videoUrl;

    private Integer status;

    private Integer length;

    private Integer contentId;

}