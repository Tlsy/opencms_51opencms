package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class AdvertPosition extends PageView<AdvertPosition> {
    private Integer id;

    private String createTime;

    private String height;

    private String isOpen;

    private String name;

    private Integer type;

    private String width;

}