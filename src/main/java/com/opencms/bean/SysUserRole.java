package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class SysUserRole extends PageView<SysUserRole> {
    private Integer id;

    private Integer roleId;

    private Integer sysUserId;

}