package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class FlowGroup extends PageView<FlowGroup> {
    private Integer id;

    private String description;

    private Integer isOpen;

    private String name;

    private Integer sort;

}