package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

import java.util.Date;

@Data
public class ContentReptile extends PageView<ContentReptile> {
    private Integer id;

    private String title;

    private String keywords;

    private String description;

    private String content;

    private Integer readNumber;

    private String source;

    private String publishTime;

    private Date createTime;

    private Integer createUser;

    private String author;

    private Integer status;

}