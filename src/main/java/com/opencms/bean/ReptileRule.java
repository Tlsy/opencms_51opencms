package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class ReptileRule extends PageView<ReptileRule> {
    private Integer id;

    private String name;

    private String coding;

    private Integer number;

    private String timeFormat;

    private String reptileUrl;

    private String contentCompletionUrl;

    private String imageCompletionUrl;

    private Integer isReptileImage;

    private String titleStart;

    private String titleEnd;

    private String descriptionStart;

    private String descriptionEnd;

    private String contentStart;

    private String contentEnd;

    private String keywordsStart;

    private String keywordsEnd;

    private String publishTimeStart;

    private String publishTimeEnd;

    private String authorStart;

    private String authorEnd;

    private String sourceStart;

    private String sourceEnd;

    private String fromUrlStart;

    private String fromUrlEnd;

    private String readNumberStart;

    private String readNumberEnd;

    private String contentAllStart;

    private String contentAllEnd;

    private String createTime;

    private String createUser;

    private Integer reptileNumber;

    private Integer status;
}