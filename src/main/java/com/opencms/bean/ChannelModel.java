package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class ChannelModel extends PageView<ChannelModel> {
    private Integer id;

    private Integer channelId;

    private Integer modelId;
    
    private String modelName;

}