package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class SysUser extends PageView<SysUser> {
    private Integer id;

    private String createTime;

    private String lastLoginTime;

    private String loginName;

    private String password;

    private String realName;

}