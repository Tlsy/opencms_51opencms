package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class ContentImage extends PageView<ContentImage>{
    private Integer id;

    private String title;

    private Integer clickNum;

    private String imageUrl;

    private Integer status;

    private Integer contentId;

}