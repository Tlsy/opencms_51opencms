package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class ContentText extends PageView<ContentText> {
    private Integer id;

    private Integer contentId;

    private String text;

}