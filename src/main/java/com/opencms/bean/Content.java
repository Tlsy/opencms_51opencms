package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class Content extends PageView<Content> {
    private Integer id;

    private String title;

    private String shortTitle;

    private Integer clickNum;

    private String createTime;

    private String createUser;

    private String description;

    private String imgUrl;

    private Integer channelId;

    private Integer flowFaceValue;

    private Integer flowValue;

    private String tags;

    private String source;

    private String author;

    private String startTime;

    private String endTime;

    private String titleColor;

    private Integer isBold;

    private Integer weight;

    private String updateTime;

    private Integer status;

    private String href;

    private Integer isTop;

    private Integer isComment;

    private Integer modelId;

    private String template;
    
    private String channelName;
    
    private String modelName;

}