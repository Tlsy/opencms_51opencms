package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class FriendLink extends PageView<FriendLink> {
    private Integer id;

    private String clickNum;

    private String createTime;

    private String imgUrl;

    private String name;

    private Integer type;

    private String url;

}