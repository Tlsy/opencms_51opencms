package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class CmsContent extends PageView<CmsContent> {
    private Integer id;

    private String clickNum;

    private String content;

    private String createTime;

    private String createUser;

    private String description;

    private String imgUrl;

    private String radioUrl;

    private String shortTitle;

    private String title;

    private Integer channelId;
    
    private String channelName;

    private Integer flowFaceValue;

    private Integer flowValue;

}