package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class FlowFace extends PageView<FlowFace> {
    private Integer id;

    private String description;

    private String name;

    private String roleIds;

    private Integer sort;

    private Integer value;

    private String roleNames;

    private Integer flowId;

}