package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class Module extends PageView<Module> {
    private Integer id;

    private String name;

    private Integer pId;
    
    private String pName;

    private Integer sort;

    private String target;

    private String href;

    private Integer moduleType;
    
    private String icon;
    
    private Boolean open;
    
    private Boolean checked;

}