package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class Flow extends PageView<Flow> {
    private Integer id;

    private String description;

    private Integer isOpen;

    private String name;

    private Integer sort;


}