package com.opencms.bean;

import com.opencms.vo.PageView;
import lombok.Data;

@Data
public class Model extends PageView<Model> {
    private Integer id;

    private String name;

    private String tempPrefix;

    private Integer hasGroupImages;

    private Integer hasVedio;

    private Integer hasContent;

    private Integer hasOptions;

    private String createTime;

    private String status;

}